const fetch = require('node-fetch');

const productSearchRoute = {
  keySearch: async function (req, res) {

    // Cookie:_fbp=fb.2.1620313602502.1555375593; _ga=GA1.3.1976765126.1620313603; _gac_UA-122361292-1=1.1620313603.Cj0KCQjwp86EBhD7ARIsAFkgakjQIov3uObHl2MAH25XmTmI_MC_hB7oMradX89VQRUskHtxiQeMUCMaAlwKEALw_wcB; cartToken=8pTrpdLgcaDEcFM3trZ-4A1620649138441; wcParameter={"BubbleActive":true,"OpenURL":false}; _gid=GA1.3.1455066593.1621255974; _gat_gtag_UA_122361292_1=1
    const code = '&code=city';
    const url = 'https://www.spree.vaypol.com.ar/api/v2/storefront/products/search?keywords='
    console.log('keysearch: ' + JSON.stringify(req.query.keyword));
    console.log('keySearch url: ' + url + req.query.keyword + code);
    const data = await fetch(url + req.query.keyword + code)
      .then(response => response.json())
      .then(data => this.data = data);
    res.send(data);
  },

  productSearch: async function (req, res) {
    const url = 'https://www.vaypol.com.ar/_next/data/Yw9LIFIBXRsS_hast-S5d/city/productos.json?';
    const query = {
      marca: 'marca=',
      keywords: 'keywords='
    }
    let data = {};
    try {
      if (req.query.marca != undefined) {
        console.log('marca=' + req.query.marca);
        data = await fetch(url + query.marca + req.query.marca)
          .then(response => response.json())
          .then(body => data = body);
        res.json( data.pageProps.initialReduxState.products);
      }
    } catch (err) {
      console.log('--> error: ' + err);
      res.json({});
    }

    try {
      if (req.query.keywords != undefined) {
        console.log('keywords=' + req.query.keywords);
        data = await fetch(url + query.keywords + req.query.keywords)
          .then(response => response.json())
          .then(body => data = body);
        res.json(data.pageProps.initialReduxState.products);
      }
    } catch (err) {
      console.log('--> error: ' + err);
      res.json({});
    }
  }
}

exports.productSearchRoute = productSearchRoute;