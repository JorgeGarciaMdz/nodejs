const express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');

const app = express();
var router = express.Router();
const prodSearchRoute = require('./route/productSearch.route');

const port = 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.use('/api/v1', router);
router.route('/searchWords').get((req, res) =>
    prodSearchRoute.productSearchRoute.keySearch(req, res));
router.route('/searchProducts').get((req, res) => prodSearchRoute.productSearchRoute.productSearch(req, res));

app.listen(port, console.log("servidor iniciado en puerto: " + port));